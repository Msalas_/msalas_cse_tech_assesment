const express = require('express');
const app = express();
const fs = require('fs');
const readline = require('readline');

async function ReadInputFile(){

    let filedata = {};
    let line_position = 0;

    const readInterface = readline.createInterface({
        input: fs.createReadStream('input.txt')
    });

    for await (const line of readInterface) {
        let coordinates = line.split(' ');
        let xVal = coordinates[0];
        let yVal = coordinates.length > 1 ? coordinates[1] : null;

        if(line_position > 1 && yVal !== null){
            filedata[`dirtPatch_xVal${xVal}_yVal${yVal}`] = {pickedUp: false};
        }
        else if(yVal == null){
            filedata["driving_directions"] = xVal.split('');
        }
        else if(line_position == 0){
            filedata["room_dimensions"] = {x: xVal, y: yVal};
        }
        else if(line_position == 1){
            filedata["hoover_position"] = {x: xVal, y: yVal};
        }         
        line_position++;
    }
    return filedata;
}


async function GetToWorkRoomba(){

    const dictionaryData = await ReadInputFile();

    let currentXPosition = dictionaryData['hoover_position']['x'];
    let currentYPosition = dictionaryData['hoover_position']['y'];
    const room_dimension_x = dictionaryData['room_dimensions']['x'] - 1;
    const room_dimension_y = dictionaryData['room_dimensions']['y'] - 1;
    const driving_directions_array = dictionaryData['driving_directions'];
    let dirtPatchesCleaned = 0;

    for(let i = 0; i < driving_directions_array.length; i++){

        let direction = driving_directions_array[i];
        switch(direction){
            case 'N':
                if(currentYPosition != room_dimension_y){
                    currentYPosition++;
                }
                break;
            case 'W':
                if(currentXPosition != 0){
                    currentXPosition--;
                }
                break;
            case 'S':
                if(currentYPosition != 0){
                    currentYPosition--;
                }
                break;
            case 'E':
                if(currentXPosition != room_dimension_x){
                    currentXPosition++;
                }
                break;
            default:
                break;
        }

        let dirtPatchLocator = `dirtPatch_xVal${currentXPosition}_yVal${currentYPosition}`;
        if(dirtPatchLocator in dictionaryData){
            if(dictionaryData[dirtPatchLocator]['pickedUp'] == false){
                dirtPatchesCleaned++;
                dictionaryData[dirtPatchLocator]['pickedUp'] = true;
            }
        }
    }

    console.log("Result: ")
    console.log(`${currentXPosition} ${currentYPosition}`);
    console.log(dirtPatchesCleaned);

}

GetToWorkRoomba();

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}... \n`));